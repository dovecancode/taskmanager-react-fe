import { useState } from 'react'
import useFetchdata from '../../hook/useFetchdata'
import { ITaskItem } from '../../types'
import TaskForm from '../TaskForm'
import TaskList from '../TaskList'

function TaskManager() {
  const { tasks, setTaskContainer } = useFetchdata()

  const [editItem, setEditItem] = useState<ITaskItem | null>(null)

  const [updatedTaskId, setUpdateTaskId] = useState<number | null>(null)

  function handelAddTask(newTask: ITaskItem) {
    setTaskContainer((prev) => ({ ...prev, tasks: [newTask, ...prev.tasks] }))
  }

  function handleDelete(id: number) {
    setTaskContainer((prev) => ({
      ...prev,
      tasks: prev.tasks.filter((item) => item.id !== id),
    }))
  }

  function handleUpdateTask(updateTaskItem: ITaskItem) {
    setTaskContainer((prev) => ({
      ...prev,
      tasks: prev.tasks.map((task) =>
        task.id === updateTaskItem.id ? updateTaskItem : task
      ),
    }))
    setUpdateTaskId(updateTaskItem.id || null)

    setTimeout(() => {
      setUpdateTaskId(null)
    }, 2000)
  }

  function handleEdit(taskItem: ITaskItem) {
    // console.log(taskItem, action)
    setEditItem(taskItem)
  }

  return (
    <div className="container">
      <TaskForm
        onTaskAdded={handelAddTask}
        editItem={editItem}
        onUpdateTask={handleUpdateTask}
      />
      <ul>
        {tasks.map((task) => (
          <TaskList
            onDelete={handleDelete}
            onEdit={handleEdit}
            key={task.id}
            taskItem={task}
            updatedTaskId={updatedTaskId}
          />
        ))}
      </ul>
    </div>
  )
}

export default TaskManager
