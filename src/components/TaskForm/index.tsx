import { ChangeEvent, FormEvent, useEffect, useState } from 'react'
import useCreateTask from '../../hook/useCreateTask'
import useEditTask from '../../hook/useEditTask'
import { ITaskItem } from '../../types'

type TaskFormProps = {
  onTaskAdded: (val: ITaskItem) => void
  onUpdateTask: (val: ITaskItem) => void
  editItem: ITaskItem | null
}

function TaskForm({ onTaskAdded, onUpdateTask, editItem }: TaskFormProps) {
  const [errMessage, setErrMessages] = useState<string>('')
  const { createTask } = useCreateTask()
  const { updateTask } = useEditTask()

  const [formData, setFormData] = useState({
    title: '',
    status: '',
  })

  const { title, status } = formData

  function handleOnChange(
    e: ChangeEvent<HTMLInputElement> | ChangeEvent<HTMLSelectElement>
  ) {
    setFormData((prev) => ({ ...prev, [e.target.name]: e.target.value }))
  }

  async function handleOnSubmit(e: FormEvent) {
    e.preventDefault()

    if (!formData.title && !formData.status) {
      return setErrMessages('Please add some task')
    }

    if (editItem) {
      const editedItem = await updateTask(formData)
      onUpdateTask(editedItem)
    } else {
      const newTask = await createTask(formData)
      onTaskAdded(newTask)
    }

    setFormData({
      title: '',
      status: '',
    })
  }

  useEffect(() => {
    if (editItem) {
      setFormData(editItem)
    }
  }, [editItem])

  return (
    <div className="container">
      <form onSubmit={handleOnSubmit}>
        <input
          type="text"
          name="title"
          value={title}
          onChange={handleOnChange}
        />
        <select name="status" value={status} onChange={handleOnChange}>
          <option value="">Select Status</option>
          <option value="pending">Pending</option>
          <option value="inprogress">In progress</option>
          <option value="completed">Completed</option>
        </select>
        <button type="submit">Submit</button>
      </form>
      {errMessage && errMessage}
    </div>
  )
}

export default TaskForm
