import useDeleteTask from '../../hook/useDeleteTask'
import { ITaskItem } from '../../types'
import styles from './style.module.css'

type TaskListProps = {
  onDelete: (val: number) => void
  onEdit: (val: ITaskItem) => void
  taskItem: ITaskItem
  updatedTaskId: number | null
}

function TaskList({
  taskItem,
  onDelete,
  onEdit,
  updatedTaskId,
}: TaskListProps) {
  const { deleteTask } = useDeleteTask()
  const statusMode: { [key: string]: string } = {
    pending: 'badge-danger',
    inprogress: 'badge-primary',
    completed: 'badge-success',
  }

  function handleDelete(id: number) {
    deleteTask(id)
    onDelete(id)
  }

  function handleEdit(taskItem: ITaskItem) {
    // console.log(taskItem)
    onEdit(taskItem)
  }

  return (
    <li
      className={`${styles['list-item']} ${
        updatedTaskId === taskItem.id ? styles.editedItem : ''
      }`}
    >
      <div>
        <p> {taskItem.title}</p>
        <span
          className={`${styles.badge} ${styles[statusMode[taskItem.status]]}`}
        >
          {taskItem.status}
        </span>
      </div>

      <div>
        <button onClick={() => taskItem.id && handleDelete(taskItem.id)}>
          Delete
        </button>
        <button onClick={() => handleEdit(taskItem)}>Edit</button>
      </div>
    </li>
  )
}

export default TaskList
