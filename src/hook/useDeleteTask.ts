function useDeleteTask() {
  async function deleteTask(id: number) {
    try {
      const response = await fetch(
        `http://localhost:8000/api/v1/tasks/${String(id)}`,
        {
          method: 'DELETE',
        }
      )
      const { data } = await response.json()

      if (!response.ok) {
        throw new Error('Error fetching all tasks')
      }

      return data[0]
    } catch (error) {
      console.log(error)
    }
  }

  return { deleteTask }
}

export default useDeleteTask
