import { useEffect, useState } from 'react'
import { ITaskItem } from '../types'

export type ITaskState = {
  tasks: ITaskItem[]
  isloading: boolean
  error: boolean
}

function useFetchdata() {
  const [taskContainer, setTaskContainer] = useState<ITaskState>({
    tasks: [],
    isloading: false,
    error: false,
  })

  useEffect(() => {
    async function fetchTasks() {
      try {
        const response = await fetch('http://localhost:8000/api/v1/tasks')
        const { data } = await response.json()

        if (!response.ok) {
          setTaskContainer((prev) => ({ ...prev, error: true }))
          throw new Error('Error fetching all tasks')
        }
        setTaskContainer((prev) => ({ ...prev, tasks: data }))
      } catch (error) {
        console.log(error)
      }
    }
    fetchTasks()
  }, [])

  const { error, isloading, tasks } = taskContainer

  return { error, isloading, tasks, setTaskContainer }
}

export default useFetchdata
