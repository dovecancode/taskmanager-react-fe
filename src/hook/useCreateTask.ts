import { ITaskItem } from '../types'

function useCreateTask() {
  async function createTask(item: ITaskItem) {
    try {
      const response = await fetch('http://localhost:8000/api/v1/tasks', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(item),
      })
      const { data } = await response.json()

      if (!response.ok) {
        throw new Error('Error fetching all tasks')
      }
      return data[0]
    } catch (error) {
      console.log(error)
    }
  }

  return { createTask }
}

export default useCreateTask
