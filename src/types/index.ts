export type ITaskItem = {
  id?: number
  title: string
  status: string
}
