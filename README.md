## Getting Started

Task manager React Version

1. Clone the project

```bash
git clone git@gitlab.com:dovecancode/taskmanager-react-fe.git

```

2. Install the dependencies:

```bash
yarn
```

3. Run the development server:

```bash
yarn dev
```
